
package vlc;


    
import java.awt.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


public class  visitor extends user{
public Integer coachid; 
public Integer timeslotid;


    public visitor  (Integer coachid,Integer timeslotid, String name, String telephone) {
            this.name = name;
            this.telephone = telephone;
            this.coachid = coachid;
            this.timeslotid = timeslotid;
            
    }
    public visitor () {
            this.name = null;
            this.telephone = null;
            this.coachid = 0;
            this.timeslotid = 0;
    }  
    
    
    
    
public void viewtimeslots(Integer coachid){
        
        
        
        JFrame frame = new JFrame("View Time Slots");
        String[] columns = {" ","Avalible Time Slot",""};
        Object[][] data = {};
        
 
        DefaultTableModel listTableModel;
        listTableModel = new DefaultTableModel(data, columns);    

       
        
        
    
        
    for(Map.Entry<Integer, String> entry:vlcdata.visittimeslots.entrySet()){    
        int key=entry.getKey();  
        String b=entry.getValue(); 
       
        boolean alreadybooked=false;
        //=======================================================
           
                for(Map.Entry<Integer, visitor> entry2:vlcdata.visitors.entrySet()){    
                   //int key2=entry2.getKey();  
                   visitor b2=entry2.getValue(); 
                   if (b2.timeslotid==key & b2.coachid==coachid){
                        
                      alreadybooked=true;
                      break;
                   }
                }
        //========================================================
        
        boolean lessthantodaydate=false;
        //=======================================================
              // 04-05-2019 09:00 AM - 09:20 AM     
            //====================================================================    
            String str = b;
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            try {
            Date date = new Date();
            Date userDate = formatter.parse(str);
            
            System.out.println(userDate); // not using toString   


            if (date.before(userDate) ) {
               
            }else {
            lessthantodaydate=true;
            System.out.println(lessthantodaydate); // not using toString   

            }
            } catch (Exception e) {
            // Invalid date was entered
            }
            
                
        
        //========================================================
     
        
        
        
        //lessthantodaydate==true & 
        
        
        if (alreadybooked==true || lessthantodaydate==true){
  
            
        }else{            
            listTableModel.addRow(new Object[]{"Book Visit",b.toString(), key});
            //JOptionPane.showMessageDialog(null, "");
        }
          
    }
        
        

        //table rendering
        
         
        JTable table;
        table = new JTable(listTableModel);
        table.getColumn(" ").setCellRenderer(new user.ButtonRenderer());
           
        
        table.getColumnModel().getColumn(2).setMinWidth(0);
        table.getColumnModel().getColumn(2).setMaxWidth(0);
        table.getColumnModel().getColumn(2).setWidth(0);
        
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            
             if (! event.getValueIsAdjusting())
                {
                
                    //eclass viewclass = new eclass();
                    //viewclass.filterclasses(null,Integer.parseInt(table.getValueAt(table.getSelectedRow(), 1).toString()));
                    System.out.println( coachid + " " + table.getValueAt(table.getSelectedRow(), 2).toString());
                    visitor newbooking = new visitor();
                    newbooking.newvisitbookings(coachid,Integer.parseInt(table.getValueAt(table.getSelectedRow(), 2).toString()));
                    
                    frame.dispose();
                }
           
            
            
        }
        });
        
        
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
 
        JLabel lblHeading = new JLabel("View Time Slots");
        lblHeading.setFont(new Font("Arial",Font.TRUETYPE_FONT,24));
 
        frame.getContentPane().setLayout(new BorderLayout());
 
        frame.getContentPane().add(lblHeading,BorderLayout.AFTER_LAST_LINE);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        
        frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
  
         
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
    }
    
    
    
    
 
public void newvisitbookings(Integer cid,Integer tid){
    
    
    
    //declare local variables for this instance
        //Integer userchoosen;
        JPanel panel = new JPanel();    
        JFrame frame2 = new JFrame("Register New Visit");
        JTextArea  coachdetails = new JTextArea (5,20);
        coachdetails.setEditable(false);
        coachdetails.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        JTextArea  tdetails = new JTextArea (5,20);
        tdetails.setEditable(false);
        tdetails.setBorder(BorderFactory.createLineBorder(Color.CYAN, 2));
        
        JTextField vname = new JTextField(20);
        JTextField vtel = new JTextField(20);
        
        
        // Creating instance of JFrame
        frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame2.setSize(300, 360);
        frame2.add(panel);
        
        frame2.setLocationRelativeTo(null);
        frame2.setLayout(null);
        
        //userchoosen=id;
        frame2.setVisible(true);
        
        
        //load components

        JLabel userLabel = new JLabel("Coach Details");
        userLabel.setBounds(10,20,80,25);
        frame2.add(userLabel);

        coachdetails.setBounds(100,20,175,75);
        frame2.add(coachdetails);

        JLabel telephoneLabel = new JLabel("Time Slot");
        telephoneLabel.setBounds(10,110,80,30);
        frame2.add(telephoneLabel);

        tdetails.setBounds(100,110,175,75);
        frame2.add(tdetails);

        JLabel jname = new JLabel("Name");
        jname.setBounds(10,195,80,25);
        frame2.add(jname);
        
        JLabel jtel = new JLabel("Telephone");
        jtel.setBounds(10,225,80,25);
        frame2.add(jtel);


        vname.setBounds(100,195,175,25);
        frame2.add(vname);
        
        vtel.setBounds(100,225,175,25);
        frame2.add(vtel);
           
        
        
        //add data from ids


        //JOptionPane.showMessageDialog(null, "class "+cclassid +" user "+tempuserid);
        vlcdata.getcoachdetails(cid);
        coachdetails.setText(vlcdata.coachdetails.name);
        
        


        tdetails.setText(vlcdata.gettimeslot(tid));
        
        

        //add bution action listener functions       
        JButton viewclassButton = new JButton("Book Visit");
        viewclassButton.setBounds(100, 260, 165, 25);
        frame2.add(viewclassButton);
        viewclassButton.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  

                //JOptionPane.showMessageDialog(null, "class "+cclassid +" user "+tempuserid);
                try{

                    vlcdata.addvisit(cid, tid, vname.getText().toString(), vtel.getText().toString());

                    JOptionPane.showMessageDialog(null, "New visit registered");
                    //




                 } catch (Exception eadd){

                     JOptionPane.showMessageDialog(null, "Unable to Register visit");

                 }



                frame2.dispose();
            }  
            });  



        JButton saveButton = new JButton("Exit");
        saveButton.setBounds(100, 290, 165, 25);
        frame2.add(saveButton);
        saveButton.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                 frame2.dispose();
            }  
            }); 
        
        
} 
    
    



public void viewreport(){
        
        JFrame framer1 = new JFrame("Visitor Report");
        
        JTextArea  tdetails = new JTextArea (5,20);
        tdetails.setEditable(false);
        tdetails.setBorder(BorderFactory.createLineBorder(Color.CYAN, 2));
        JScrollPane scrollPane = new JScrollPane(tdetails);
        
        
        
        tdetails.setText("=================================================================");
        
        for(Map.Entry<Integer, coach> entry:vlcdata.coaches.entrySet()){    
        int key=entry.getKey();  
        coach b=entry.getValue(); 
       
        //=======================================================
         
        tdetails.setText(tdetails.getText() + "\n" + b.name);
        tdetails.setText(tdetails.getText() + "\n" + "---------------------------------------------------------------------");
        
                for(Map.Entry<Integer, visitor> entry2:vlcdata.visitors.entrySet()){    
                    //int key2=entry2.getKey();  
                    visitor b2=entry2.getValue(); 
                   if (b2.coachid==key){
                        
                       
                       tdetails.setText(tdetails.getText() + "\n\t " + b2.name + "\t\t" + b2.telephone  + "\t\t" + vlcdata.gettimeslot(b2.timeslotid));
                    
                   
                   }

                }
        tdetails.setText(tdetails.getText() + "\n" + "---------------------------------------------------------------------");
        
        
        }

       
        
         
       
        
        
        
        
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framer1.add(scrollPane);
        framer1.setSize(900, 500);
        framer1.setLocationRelativeTo(null);
        framer1.setVisible(true);
        
    }
    
    
    
}
