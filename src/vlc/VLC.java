/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vlc;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import java.awt.Dimension;
import javax.swing.JOptionPane;



public class VLC extends JFrame{

    static vlcdata vlcdatavar= new vlcdata();
    
    public VLC() 
    {
    //super("Venue Leisure Centre"); 
    JFrame frame1 = new JFrame("Venue Leisure Centre");
    frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame1.setSize(820,550);
    
    //frame1.setResizable(false);
    //frame1.setLayout(new FlowLayout());

    //this.getContentPane().setLayout(new FlowLayout());
    //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 
    JButton viewmemberbtn = new JButton("View Member Details"); // 
    JButton viewcoachbtn = new JButton("View Coaches"); // 
    JButton viewclassesbtn = new JButton("View Classes By Interest"); // 
    JButton newbookingbtn = new JButton("New Booking"); // 
    JButton newmemberbtn = new JButton("New Member"); // 
    JButton viewbookingbtn = new JButton("View Booking"); // 
    JButton viewreportbtn = new JButton("View Reports"); // 
    JButton visitorbtn = new JButton("New Visitor Appointment By Interest"); // 
    
    viewmemberbtn.setPreferredSize(new Dimension(250, 150));
    viewcoachbtn.setPreferredSize(new Dimension(250, 150));
    viewclassesbtn.setPreferredSize(new Dimension(250, 150));
    newbookingbtn.setPreferredSize(new Dimension(250, 150));
    newmemberbtn.setPreferredSize(new Dimension(250, 150));
    viewbookingbtn.setPreferredSize(new Dimension(250, 150));
    viewreportbtn.setPreferredSize(new Dimension(250, 150));
    visitorbtn.setPreferredSize(new Dimension(250, 150));
    
    //Creating the MenuBar and adding components
    JMenuBar menub = new JMenuBar();
    JMenu m1 = new JMenu("File");
    menub.add(m1);
    JMenuItem m11 = new JMenuItem("Exit");
    m1.add(m11);
    m11.addActionListener(new closeprogram());
    
    //layout
    JPanel panel = new JPanel();
    panel.setBackground(Color.darkGray);
    panel.setSize(800,550);
    //panel.add(headerpanel,BorderLayout.NORTH);
    panel.add(viewmemberbtn);
    panel.add(viewcoachbtn);
    
    panel.add(viewclassesbtn);
    panel.add(newbookingbtn);
    panel.add(newmemberbtn);
    panel.add(viewbookingbtn);
    panel.add(viewreportbtn);
    panel.add(visitorbtn);
    
     // add an ActionListener to respond to a button click
    viewmemberbtn.addActionListener(new bookingbtnClickListener());
    
    viewcoachbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    coach viewcoach = new coach();
                    viewcoach.viewcoaches();
                    
            }  
            });  

    // 
    
    viewclassesbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                     //viewcoaches viewcoach = new viewcoaches();
                    eclass viewclass = new eclass();
                    viewclass.viewclasses();
            }  
            });  

    // 
    newmemberbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                     member addmember = new member();
                     addmember.newmember();
                    
            }  
            });  

    //
    visitorbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    coach viewcoach = new coach();
                    viewcoach.viewvisitorcoaches();
                    
            }  
            });  
    
    //
    viewbookingbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                     
                    try
                    {
                      String uid=JOptionPane.showInputDialog("User Id", "Enter Your User Id");  
                      boolean usermemberlogin;
                      usermemberlogin=false;

                      usermemberlogin  = VLC.vlcdatavar.checkmembers(Integer.parseInt(uid));
                      if (usermemberlogin == true) {
                            //frame.setVisible(true);

                            
                            booking vbooking = new booking();
                            vbooking.viewbookings(Integer.parseInt(uid));
                            
                    
                    
                      }else {
                            JOptionPane.showMessageDialog(null, "User Not Found");

                            } 
                      
                    }catch (Exception e2) {
                            JOptionPane.showMessageDialog(null,"User Not Found");

                    }
                
                
                    
            }  
            });  
    
    //
    // 
    newbookingbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                     
                
                            try
            {
              String uid=JOptionPane.showInputDialog("User Id", "Enter Your User Id");  
              boolean usermemberlogin;
              usermemberlogin=false;
              
              usermemberlogin  = VLC.vlcdatavar.checkmembers(Integer.parseInt(uid));
              if (usermemberlogin == true) {
                    //frame.setVisible(true);
                    
                    
                    
                    
                    //app.viewmember(Integer.parseInt(uid));
                    
                eclass viewclassesall = new eclass();
                
                viewclassesall.parseuid=Integer.parseInt(uid);
                viewclassesall.filterclasses("all", 0);
                    
                    
                    
                    
                    
              }else {
                    JOptionPane.showMessageDialog(null, "User Not Found");

                    } 
            }catch (Exception e2) {
                    JOptionPane.showMessageDialog(null,"User Not Found");

            }
                
                    
            }  
            });  

    // 
    // 
    viewreportbtn.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                     
                JPanel panel22 = new JPanel();    
                JFrame frame2 = new JFrame("Reports");
        
                JButton report1 = new JButton("Visitor Report"); // 
                JButton report2 = new JButton("Class Status"); // 
                JButton report3 = new JButton("Class Students"); // 
    
                
                frame2.setSize(570,90);
                frame2.setLocationRelativeTo(null);
                
                
                panel22.setBackground(Color.darkGray);
                panel22.setSize(270,270);
                panel22.add(report1);
                panel22.add(report2);
                panel22.add(report3);
                
                frame2.add(panel22);
    

    
    
                frame2.setVisible(true);
                
                report1.addActionListener(new ActionListener(){  
                public void actionPerformed(ActionEvent e){  
                        
                        visitor viewr1 = new visitor();
                        viewr1.viewreport();
                        
                        frame2.dispose();

                }  
                }); 
                
                
                report2.addActionListener(new ActionListener(){  
                public void actionPerformed(ActionEvent e){  
                        
                        eclass viewr1 = new eclass();
                        viewr1.viewreport();
                        
                        frame2.dispose();

                }  
                });
                      
                                
                report3.addActionListener(new ActionListener(){  
                public void actionPerformed(ActionEvent e){  
                        
                        eclass viewr1 = new eclass();
                        viewr1.viewreport2();
                        
                        frame2.dispose();

                }  
                });
                
                
                
                
                
                
                
                
                
                    
            }  
            });  

    // 
    
    
    
    
    frame1.getContentPane().add(BorderLayout.NORTH, menub);
    frame1.add(panel);
    frame1.setLocationRelativeTo(null);
    frame1.setVisible(true);
    //setSize(650, 650); // set frame size to width 400 pixels, height 200
    }

    
        
    
    public static void main(String[] args) {
        // TODO code application logic here
        VLC app = new VLC();
         
        //app.setVisible(true); 
         
    }
    
}



class bookingbtnClickListener implements ActionListener {

    public bookingbtnClickListener () {
        super ();
    }
    public void actionPerformed(ActionEvent e) {
        //JOptionPane.showMessageDialog (null,"There was an error in your program","Program error",JOptionPane.ERROR_MESSAGE);
    
            try
            {
              String uid=JOptionPane.showInputDialog("User Id", "Enter Your User Id");  
              boolean usermemberlogin;
              usermemberlogin=false;
              
              usermemberlogin  = VLC.vlcdatavar.checkmembers(Integer.parseInt(uid));
              if (usermemberlogin == true) {
                    //frame.setVisible(true);
                    
                    member app = new member();
                    
                    app.viewmember(Integer.parseInt(uid));
              }else {
                    JOptionPane.showMessageDialog(null, "User Not Found");

                    } 
            }catch (Exception e2) {
                    JOptionPane.showMessageDialog(null,"User Not Found");

            }
    }
}


class closeprogram implements ActionListener {
    public closeprogram () {
        super ();
    }
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}

