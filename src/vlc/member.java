/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vlc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class  member extends user{

        
    public member (String name,String address, String telephone) {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
    }
    
    public member () {
            this.name = null;
            this.telephone = null;
            this.address = null;
    }
    
    
    public void viewmember (Integer id) {
         
        //declare local variables for this instance
        Integer userchoosen;
        JPanel panel = new JPanel();    
        JFrame frame = new JFrame("Member Details");
        JTextField addressText = new JTextField(20);
        JTextField telephoneText = new JTextField(20);
        JTextField userText = new JTextField(20);
        
        
        // Creating instance of JFrame
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(300, 260);
        frame.add(panel);
        
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);
        
        userchoosen=id;
        frame.setVisible(true);
        
        
        //load components
        
        JLabel userLabel = new JLabel("User Name");
        userLabel.setBounds(10,20,80,25);
        frame.add(userLabel);
        
        userText.setBounds(100,20,165,25);
        frame.add(userText);
        
        JLabel telephoneLabel = new JLabel("Telephone");
        telephoneLabel.setBounds(10,50,80,25);
        frame.add(telephoneLabel);

        telephoneText.setBounds(100,50,165,25);
        frame.add(telephoneText);
                
        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(10,80,80,25);
        frame.add(addressLabel);

                
        addressText.setBounds(100,80,165,25);
        frame.add(addressText);
         
        //add bution action listener functions       
        JButton viewclassButton = new JButton("View Booked Classes");
        viewclassButton.setBounds(100, 140, 165, 25);
        frame.add(viewclassButton);
        viewclassButton.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    booking vbooking = new booking();
                    vbooking.viewbookings(userchoosen);
                    frame.dispose();
            }  
            });  
        
      
   
        
        
        JButton saveButton = new JButton("Save and Close");
        saveButton.setBounds(100, 180, 165, 25);
        frame.add(saveButton);
        saveButton.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                
                    boolean saved;
                    saved=vlcdata.updatemember(userchoosen,userText.getText(),addressText.getText(),telephoneText.getText());
                    if (saved == true) {
                    JOptionPane.showMessageDialog(null, "Data Saved");
                    }else {
                    JOptionPane.showMessageDialog(null, "Data not saved");
                    }       
                    frame.dispose();
            }  
            });  
        
        
        
       //set gui values
       vlcdata.getuserdetails(id); 
       userText.setText(vlcdata.userdetails.name); 
       //name=vlcdata.userdetails.name;
       addressText.setText(vlcdata.userdetails.address);
       telephoneText.setText(vlcdata.userdetails.telephone);
   
         
        
    }
    
    
    
    public String testviewmember(Integer tid){

       vlcdata vtest=new vlcdata();
       vtest.getuserdetails(tid);
       return vtest.userdetails.name; 
   
    }
    
    
    public void newmember () {
         
        //declare local variables for this instance
        //Integer userchoosen;
        JPanel panel = new JPanel();    
        JFrame frame = new JFrame("Register New Member");
        JTextField addressText = new JTextField(20);
        JTextField telephoneText = new JTextField(20);
        JTextField userText = new JTextField(20);
        
        
        // Creating instance of JFrame
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(300, 260);
        frame.add(panel);
        
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);
        
        //userchoosen=id;
        frame.setVisible(true);
        
        
        //load components
        
        JLabel userLabel = new JLabel("User Name");
        userLabel.setBounds(10,20,80,25);
        frame.add(userLabel);
        
        userText.setBounds(100,20,165,25);
        frame.add(userText);
        
        JLabel telephoneLabel = new JLabel("Telephone");
        telephoneLabel.setBounds(10,50,80,25);
        frame.add(telephoneLabel);

        telephoneText.setBounds(100,50,165,25);
        frame.add(telephoneText);
                
        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(10,80,80,25);
        frame.add(addressLabel);

                
        addressText.setBounds(100,80,165,25);
        frame.add(addressText);
         
        //add bution action listener functions       
        JButton viewclassButton = new JButton("Register");
        viewclassButton.setBounds(100, 140, 165, 25);
        frame.add(viewclassButton);
        viewclassButton.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    
                try{
                   vlcdata.addmembers(telephoneText.getText(), addressText.getText(), telephoneText.getText());
                   JOptionPane.showMessageDialog(null, "Member registered");
                   Integer tempid=vlcdata.lastmember();
                   JOptionPane.showMessageDialog(null, "Your Member id is: " +tempid +" ");
                   
                } catch (Exception eadd){
                  
                    JOptionPane.showMessageDialog(null, "Unable to add user");
                    
                }
                
                frame.dispose();
            }  
            });  
        
      
   
        
        
        JButton saveButton = new JButton("Exit");
        saveButton.setBounds(100, 180, 165, 25);
        frame.add(saveButton);
        saveButton.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                 frame.dispose();
            }  
            });  
        
        
    }
   
    
    
    
}
