/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vlc;

import java.util.*;
import java.util.Map.Entry;




public class vlcdata {
   
    static Map<Integer, member> members = new LinkedHashMap<> ();
    
    
    static Map<Integer, coach> coaches = new LinkedHashMap<> ();
    static Map<Integer, eclass> eclasses = new LinkedHashMap<> ();
    static Map<Integer, booking> bookings = new LinkedHashMap<> ();
    
    static Map<Integer, String> visittimeslots = new LinkedHashMap<> ();
    static Map<Integer, visitor> visitors = new LinkedHashMap<> ();
    
            
    static member isuser;
    static member userdetails;
    static member setuserdetails;
    static coach coachdetails;
    static eclass classdetails;
    
    
    public vlcdata () {
    
    //add members   
    members.put (1, new member("Amanda", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (2, new member("Alex", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (3, new member("James", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (4, new member("Daniel", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (5, new member("Katie", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (6, new member("Lee", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (7, new member("Vijay", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (8, new member("Abi", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (9, new member("Jai", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (10, new member("Kai", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (11, new member("Peter", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (12, new member("Michael", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (13, new member("JJ", "12 noway road, hatfield, ha1 2db","078787878"));  
    members.put (14, new member("Aman", "12 noway road, hatfield, ha1 2db","078787878"));
    members.put (15, new member("Jessie", "12 noway road, hatfield, ha1 2db","078787878"));
     
    //add coach   - advanced swimming, gymnastics, badminton
    coaches.put (1, new coach("Peter","089909900","200 way to", true, false, true)); 
    coaches.put (2, new coach("Alex","089909900","200 way to", false, true, false));
    coaches.put (3, new coach("Howard","089909900","200 way to", true, false, true));
    coaches.put (4, new coach("Jessica","089909900","200 way to", false, true, false));
    coaches.put (5, new coach("Daniela","089909900","200 way to", true, true, false));
     
    //add classes   - pool,studio a, studio b, studio c, gym
    //add classes   - advanced swimming, gymnastics, badminton
    eclasses.put (1, new eclass(1, "Advanced Swimming", "POOL", "15-04-2019","18:00","20:00","Swimming"));
    eclasses.put (2, new eclass(1, "Advanced Swimming", "POOL", "16-05-2019","18:00","20:00","Swimming")); 
    eclasses.put (3, new eclass(1, "Advanced Swimming", "POOL", "17-05-2019","18:00","20:00","Swimming")); 
    eclasses.put (4, new eclass(3, "Advanced Swimming", "POOL", "18-05-2019","18:00","20:00","Swimming")); 
    eclasses.put (5, new eclass(3, "Advanced Swimming", "POOL", "19-05-2019","18:00","20:00","Swimming")); 
    eclasses.put (6, new eclass(3, "Advanced Swimming", "POOL", "20-05-2019","18:00","20:00","Swimming")); 
    eclasses.put (7, new eclass(5, "Advanced Swimming", "POOL", "21-05-2019","18:00","20:00","Swimming")); 
    eclasses.put (8, new eclass(5, "Badminton", "studio b", "20-05-2019","18:00","20:00","Badminton")); 
    eclasses.put (9, new eclass(3, "Badminton", "studio c", "21-05-2019","18:00","20:00","Badminton")); 
    eclasses.put (10, new eclass(5, "Weights gym", "gym", "20-05-2019","18:00","20:00","Gymnastics")); 
    eclasses.put (11, new eclass(5, "Weights gym", "gym", "21-05-2019","18:00","20:00","Gymnastics")); 
    
    //add booking   id,classid,memberid
    bookings.put (1, new booking(1,1,"15-05-2019 06:00 PM","notes"));
    bookings.put (2, new booking(2,1,"15-05-2019 06:00 PM","notes"));
    bookings.put (3, new booking(3,1,"15-05-2019 06:00 PM","notes"));
    bookings.put (4, new booking(1,2,"15-05-2019 06:00 PM","notes"));
    bookings.put (5, new booking(2,2,"15-05-2019 06:00 PM","notes"));
    
    bookings.put (6, new booking(1,3,"15-05-2019 06:00 PM","notes"));
    bookings.put (7, new booking(1,4,"15-05-2019 06:00 PM","notes"));
    bookings.put (8, new booking(1,5,"15-05-2019 06:00 PM","notes"));
    bookings.put (9, new booking(1,6,"15-05-2019 06:00 PM","notes"));
    bookings.put (10, new booking(1,7,"15-05-2019 06:00 PM","notes"));
  
    bookings.put (11, new booking(1,8,"15-05-2019 06:00 PM","notes"));
    bookings.put (12, new booking(1,9,"15-05-2019 06:00 PM","notes"));
    bookings.put (13, new booking(1,10,"15-05-2019 06:00 PM","notes"));
    bookings.put (14, new booking(2,11,"15-05-2019 06:00 PM","notes"));
    bookings.put (15, new booking(2,12,"15-05-2019 06:00 PM","notes"));
  
     
    //add timeslots   id,date-time
    visittimeslots.put (1,"04-05-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (2,"04-05-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (3,"04-05-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (4,"06-05-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (5,"06-05-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (6,"06-05-2019 05:40 PM - 06:00 PM");
    
    visittimeslots.put (7,"11-05-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (8,"11-05-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (9,"11-05-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (10,"13-05-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (11,"13-05-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (12,"13-05-2019 05:40 PM - 06:00 PM");
    
    visittimeslots.put (13,"18-05-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (14,"18-05-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (15,"18-05-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (16,"20-05-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (17,"20-05-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (18,"20-05-2019 05:40 PM - 06:00 PM");
    
    visittimeslots.put (19,"25-05-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (20,"25-05-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (21,"25-05-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (22,"27-05-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (23,"27-05-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (24,"27-05-2019 05:40 PM - 06:00 PM");  
    
    
    
    visittimeslots.put (25,"01-06-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (26,"01-06-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (27,"01-06-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (28,"03-06-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (29,"03-06-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (30,"03-06-2019 05:40 PM - 06:00 PM");  
    
    
    visittimeslots.put (31,"08-06-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (32,"08-06-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (33,"08-06-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (34,"15-06-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (35,"15-06-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (36,"15-06-2019 05:40 PM - 06:00 PM");  
    
    visittimeslots.put (37,"17-06-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (38,"17-06-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (39,"17-06-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (40,"22-06-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (41,"22-06-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (42,"22-06-2019 05:40 PM - 06:00 PM");  
    
    visittimeslots.put (43,"24-06-2019 09:00 AM - 09:20 AM");
    visittimeslots.put (44,"24-06-2019 09:20 AM - 09:40 AM");
    visittimeslots.put (45,"24-06-2019 09:40 AM - 10:00 AM");
    
    visittimeslots.put (46,"29-06-2019 05:00 PM - 05:20 PM");
    visittimeslots.put (47,"29-06-2019 05:20 PM - 05:40 PM");
    visittimeslots.put (48,"29-06-2019 05:40 PM - 06:00 PM");  
    
    
    //ADD VISITER AOOKINGS - id,coachid,timeslotid,name,telehone
    visitors.put (1, new visitor(1,10,"Alpha","08956412"));
    visitors.put (2, new visitor(1,11,"Alpha2","08956412"));
    visitors.put (3, new visitor(2,12,"Alpha3","08956412"));
    visitors.put (4, new visitor(3,13,"Alpha4","08956412"));
    visitors.put (5, new visitor(4,14,"Alpha5","08956412"));
   
   
    
    }
    
    
    
    
    public static void addvisit(Integer coachid,Integer timeid,String name,String tel){
              
    Integer counter=0;
        
    final Set<Entry<Integer, visitor>> mapValues = visitors.entrySet();
    
    final int maplength = mapValues.size();
    final Entry<Integer,visitor>[] test = new Entry[maplength];
    mapValues.toArray(test);

    
    
    counter=test[maplength-1].getKey()+1;
    
    visitors.put (counter, new visitor(coachid,timeid,name, tel));  
       
    }
    
    
    
    
    
    
    
            
     public static boolean checkmemberclasstime(Integer memberid,String tempclassdate,String tempstarttime){
        boolean found=false;     
        for(Map.Entry<Integer, booking> entry2:bookings.entrySet()){    

            booking b2=entry2.getValue(); 
            if (b2.memberid == memberid){
                    
                eclass tempcheck = eclasses.get(b2.classid);
                if (tempcheck.classdate == tempclassdate){
                    
                    if (tempcheck.startime == tempstarttime){
                        found=true;
                    }
                    
                }
                
                
             }


        }
  
        return found;
    }
    
     
     
     
    
    public static boolean checkmembers(int varuserid){
              
        isuser = members.get(varuserid);
        System.out.println (isuser.name);
       
        if (isuser.name == null){
            return false;
        }else{
           //JOptionPane.showMessageDialog(null, varuserid);
           return true; 
        }
        
    }
    
    public static void getuserdetails(int varuserid)
    {
              
        userdetails = members.get(varuserid);
       
    }
    
    
   
    
    
    public static void getcoachname(int varuserid)
    {
              
        coachdetails = vlcdata.coaches.get(varuserid);
        
   
    }
    
      public static String getcoachnames(int varuserid)
    {
              
        coach temp= vlcdata.coaches.get(varuserid);
        return temp.name;
   
    }
    
      
    public static Integer getclassstudentlist(int classid)
    {
              
                Integer count=0;
                for(Map.Entry<Integer, booking> entry2:vlcdata.bookings.entrySet()){    
                      
                    booking b2=entry2.getValue(); 
                    if (b2.classid == classid){
                        
                       
                        
                        count=count+1;
                   
                   }

                }
        return count;
   
    } 
      
      
      
      
    public static void addmembers(String name, String address,String telephone){
              
    Integer counter=0;
        
    final Set<Entry<Integer, member>> mapValues = members.entrySet();
    
    final int maplength = mapValues.size();
    final Entry<Integer,member>[] test = new Entry[maplength];
    mapValues.toArray(test);

    
    
    counter=test[maplength-1].getKey()+1;
    
    members.put (counter, new member(name, address,telephone));  
       
    }
    
    
    //add booking   id,classid,memberid,date,notes
    public static void addbooking(Integer classid,Integer memberid,String date,String notes){
              
    Integer counter=0;
        
    final Set<Entry<Integer, booking>> mapValues = bookings.entrySet();
    
    final int maplength = mapValues.size();
    final Entry<Integer,booking>[] test = new Entry[maplength];
    mapValues.toArray(test);

    
    
    counter=test[maplength-1].getKey()+1;
    
    bookings.put (counter, new booking(classid,memberid,date, notes));  
       
    }
    
    
    
    public static Integer lastmember(){
              
    Integer counter=0;
    final Set<Entry<Integer, member>> mapValues = members.entrySet();
    final int maplength = mapValues.size();
    final Entry<Integer,member>[] test = new Entry[maplength];
    mapValues.toArray(test);
    counter=test[maplength-1].getKey();
    return counter;
    
    }
    
    public static boolean updatemember(Integer user,String name,String address,String telephone){
        
        members.put (user, new member(name, address,telephone));
        
        return true;
    }
    
    
    
    public static void getcoachdetails(int varuserid)
    {
              
        coachdetails = coaches.get(varuserid);
        
   
    }
    
    
    public static void getclassdetails(int varuserid)
    {
              
        classdetails = eclasses.get(varuserid);
        
   
    }
    
    
    
    
    
    public static boolean deletebooking(Integer id){
        
        bookings.remove(id);
        
        return true;
    }
    
    
    public static String gettimeslot(int id)
    {
              
        return vlcdata.visittimeslots.get(id);
        
   
    }
    
}
















