
package vlc;

import java.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.*;
import javax.swing.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;




public class  booking extends user{
    
public String coachname;
public String classname;
public String classdate;
public String starttime;
public String endtime;
public String classstatus;
public String classroom;
public Integer classid;
public Integer memberid; 
public String bookingdatetime;
public String notes;
public Integer coachid;

public String tempclassdate;
public String tempstarttime;

    public booking  (Integer classid, Integer memberid,String bookingdatetime,String notes) {
            this.classid = classid;
            this.memberid = memberid;
            this.bookingdatetime = bookingdatetime;
            this.notes = notes;
    }
    public booking () {
            this.classid = null;
            this.memberid = null;
            this.bookingdatetime = null;
            this.notes = null;
    }
    
    public void viewbookings(Integer userid){
        
        JFrame frame = new JFrame("View Booked Classes");
        String[] columns = {" ","Booking Id", "Course Name", "Course Start date", "Status",
                            "Course Room", "Coach Name", "Member Name"};
        Object[][] data = {};
        
 
        DefaultTableModel listTableModel;
        listTableModel = new DefaultTableModel(data, columns);    
        
        
    
        
        for(Map.Entry<Integer, booking> entry:vlcdata.bookings.entrySet()){    
        int key=entry.getKey();  
        booking b=entry.getValue(); 
       
        
            if( userid == b.memberid){
                
                //=======================================================
                vlcdata.getclassdetails(b.classid); 
                this.classname=(vlcdata.classdetails.name);
                
                
                this.classdate=(vlcdata.classdetails.classdate);
                this.starttime=(vlcdata.classdetails.startime);
                this.endtime=(vlcdata.classdetails.endtime);
                
                
                
                //date1 = sdf.format(this.classdate);
                
                //====================================================================    
                String str = this.classdate;
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                try {
                    Date date = new Date();
                    Date userDate = formatter.parse(str);

                    if (date.before(userDate) ) {
                        this.classstatus="Booked"; // not using toString   
                    }else {
                        this.classstatus="Attending";
                    }
                } catch (Exception e) {
                    // Invalid date was entered
                }

                //====================================================================
                SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
                try {
                    Date ten = parser.parse("18:00");
                    Date eighteen = parser.parse("20:00");
                    Date userDate = parser.parse("14:00");

                    if (userDate.after(ten) && userDate.before(eighteen)) {
                        System.out.println(ten); // not using toString   

                    }
                } catch (Exception e) {
                    // Invalid date was entered
                }
                //=====================================================================


                
                this.classroom=(vlcdata.classdetails.classroom);
                this.coachid=(vlcdata.classdetails.coachid);
                
                
                //get coach name from class id
                //=======================================================
                vlcdata.getcoachdetails(this.coachid); 
                this.coachname=(vlcdata.coachdetails.name);
               
                
                //get membername
                //=======================================================
                vlcdata.getuserdetails(userid); 
                this.name=(vlcdata.userdetails.name); 
                //=======================================================  
                    
                
                listTableModel.addRow(new Object[]{"Delete", key, this.classname, this.classdate, this.classstatus, this.classroom, this.coachname,this.name});
       
                 
            }
        
        }

        
        
        
            
            
            
        JTable table;
        table = new JTable(listTableModel);
        table.getColumn(" ").setCellRenderer(new ButtonRenderer());
        
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog (null, "Would You Like to delete booking id: " +table.getValueAt(table.getSelectedRow(), 1).toString() ,"Warning",dialogButton);
            if(dialogResult == JOptionPane.YES_OPTION){
                
            if (table.getValueAt(table.getSelectedRow(), 4).toString()=="Attending"){
                
                
              JOptionPane.showMessageDialog(null, "Current booking can't be deleted because you are already attending the course");
              
            }else{
                
            
              //JOptionPane.showMessageDialog(null, table.getValueAt(table.getSelectedRow(), 1).toString());
              String ids;
              ids=table.getValueAt(table.getSelectedRow(), 1).toString();
              Integer bid= Integer.valueOf(ids);
              
              vlcdata.deletebooking(bid);
              
              JOptionPane.showMessageDialog(null, "Booking cancelled !!!!..");
              
              frame.dispose();
              
              
            }
            }
            //System.out.println(table.getValueAt(table.getSelectedRow(), 1).toString());
            
        }
        });
        
        
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
 
        JLabel lblHeading = new JLabel("View Booked classes");
        lblHeading.setFont(new Font("Arial",Font.TRUETYPE_FONT,24));
 
        frame.getContentPane().setLayout(new BorderLayout());
 
        frame.getContentPane().add(lblHeading,BorderLayout.AFTER_LAST_LINE);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        
        frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
  
         
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    
    }
        
        
   
    
public void newbookings(Integer cclassid,Integer userid){
    
    Integer tempuserid=userid;
    //String tempcoachname;
    
    
    boolean userfound=false;
    
    //JOptionPane.showMessageDialog(null, "class "+cclassid +" user "+tempuserid);
             
    
    if (tempuserid==0){
       
        
            try
            {
              String uid=JOptionPane.showInputDialog("User Id", "Enter Your User Id");  
              boolean usermemberlogin;
              usermemberlogin=false;
              
              usermemberlogin  = VLC.vlcdatavar.checkmembers(Integer.parseInt(uid));
              if (usermemberlogin == true) {
                    //frame.setVisible(true);
                    
                    tempuserid=Integer.parseInt(uid);
                    userfound=true;
                    
              }else {
                    JOptionPane.showMessageDialog(null, "User Not Found");
                    //close instane
                    

                    } 
            }catch (Exception e2) {
                    JOptionPane.showMessageDialog(null,"User Not Found");
                    //close instane
                    
                    
            }
        
        
        
    }else{
        
            userfound=true;
        
    }

    
   
    //declare local variables for this instance
        //Integer userchoosen;
        JPanel panel = new JPanel();    
        JFrame frame = new JFrame("Register New Booking");
        JTextArea  classdetails = new JTextArea (5,20);
        classdetails.setEditable(false);
        classdetails.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        JTextArea  memberdetails = new JTextArea (5,20);
        memberdetails.setEditable(false);
        memberdetails.setBorder(BorderFactory.createLineBorder(Color.CYAN, 2));
        
        JTextField notes = new JTextField(20);
        
        
        // Creating instance of JFrame
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(300, 360);
        frame.add(panel);
        
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);
        
        //userchoosen=id;
        frame.setVisible(true);
        
        if (userfound==false){frame.dispose();}else{
                            //load components

                            JLabel userLabel = new JLabel("Class Details");
                            userLabel.setBounds(10,20,80,25);
                            frame.add(userLabel);

                            classdetails.setBounds(100,20,175,75);
                            frame.add(classdetails);

                            JLabel telephoneLabel = new JLabel("Member");
                            telephoneLabel.setBounds(10,110,80,30);
                            frame.add(telephoneLabel);

                            memberdetails.setBounds(100,110,175,75);
                            frame.add(memberdetails);

                            JLabel addressLabel = new JLabel("Notes");
                            addressLabel.setBounds(10,195,80,25);
                            frame.add(addressLabel);


                            notes.setBounds(100,195,175,25);
                            frame.add(notes);
                            
                            //add data from ids
                            
                            
                            //JOptionPane.showMessageDialog(null, "class "+cclassid +" user "+tempuserid);
                            vlcdata.getclassdetails(cclassid);
                            classdetails.setText(vlcdata.classdetails.name);
                            classdetails.setText(classdetails.getText() + "\nat room: " + vlcdata.classdetails.classroom);
                            classdetails.setText(classdetails.getText() + "\n" + vlcdata.classdetails.classdate);
                            classdetails.setText(classdetails.getText() + "\n" + vlcdata.classdetails.startime + " - " + vlcdata.classdetails.endtime);
                            
                            this.tempclassdate=vlcdata.classdetails.classdate;
                            this.tempstarttime=vlcdata.classdetails.startime;
                            
                            
                            //classdetails.setText(classdetails.getText()+ "\na string or even an arraylist");
                            vlcdata.getuserdetails(tempuserid);
                            //vlcdata.coachdetails(tempuserid);
                            
                            memberdetails.setText(vlcdata.userdetails.name);
                            memberdetails.setText(memberdetails.getText() + "\n" + vlcdata.userdetails.telephone);
                            memberdetails.setText(memberdetails.getText() + "\n" + vlcdata.userdetails.address);
                            
                            //
                            this.classid=cclassid;
                            this.memberid=tempuserid;
                            //


                            //add bution action listener functions       
                            JButton viewclassButton = new JButton("Register");
                            viewclassButton.setBounds(100, 230, 165, 25);
                            frame.add(viewclassButton);
                            viewclassButton.addActionListener(new ActionListener(){  
                                public void actionPerformed(ActionEvent e){  

                                    //JOptionPane.showMessageDialog(null, "class "+cclassid +" user "+tempuserid);
                                    try{
                                        
                                        SimpleDateFormat datereg = new SimpleDateFormat("dd-MM-yyyy HH:mm");

                                        //validations 1 max students
                                        
                                        boolean maxnumberreached=false;
                                        if(vlcdata.getclassstudentlist(classid)>=10){
                                        maxnumberreached=true;
                                        }
                                        
                                        //validations 2 another class same time
                                        
                                        boolean sametimeclass=false;
                                        if(vlcdata.checkmemberclasstime(memberid,tempclassdate,tempstarttime)==true){
                                        sametimeclass=true;
                                        }
                                        
                                        
                                        if(maxnumberreached==true || sametimeclass==true ){
                                        
                                            JOptionPane.showMessageDialog(null, "Your Booking can't be registered either max number of students is reached or you can attending another class same time.");
                                       
                                            
                                        }else{
                                        
                                        vlcdata.addbooking(classid, memberid, datereg.toString(), notes.getText());
                                        JOptionPane.showMessageDialog(null, "New Booking registered");
                                       
                                        }
                                        
                                        //
                                        
                                        
                                        

                                     } catch (Exception eadd){

                                         JOptionPane.showMessageDialog(null, "Unable to Register Booking");

                                     }
                                    
                                    
                                    
                                    frame.dispose();
                                }  
                                });  



                            JButton saveButton = new JButton("Exit");
                            saveButton.setBounds(100, 270, 165, 25);
                            frame.add(saveButton);
                            saveButton.addActionListener(new ActionListener(){  
                                public void actionPerformed(ActionEvent e){  
                                     frame.dispose();
                                }  
                                }); 
        
        }//close if on userfound
        
        
        
        
    
} 
    
    
  
}
