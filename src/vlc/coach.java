/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vlc;

    
import java.awt.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


public class  coach extends user{
public boolean teachswimming; 
public boolean teachbadminton;
public boolean teachgynnastics;
    public coach  (String name,String telephone, String address, boolean teachswimming, boolean teachbadminton,boolean teachgynnastics ) {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
            this.teachswimming = teachswimming;
            this.teachbadminton = teachbadminton;
            this.teachgynnastics = teachgynnastics;
    }
    public coach () {
            this.name = null;
            this.telephone = null;
            this.address = null;
            this.teachswimming = false;
            this.teachbadminton = false;
            this.teachgynnastics = false;
    }
    
    public void viewcoaches(){
        
    JFrame frame1 = new JFrame("View coaches");
    
    frame1.setSize(500,270);
    
    //advanced swimming, gymnastics, badminton
    JButton viewswim = new JButton("Swimming Coaches"); // 
    JButton viewgym = new JButton("Gym Coaches"); // 
    JButton viewbadmin = new JButton("Badminton Coaches"); // 
    JButton viewall = new JButton("View All"); // 
     
    viewswim.setPreferredSize(new Dimension(200, 100));
    viewgym.setPreferredSize(new Dimension(200, 100));
    viewbadmin.setPreferredSize(new Dimension(200, 100));
    viewall.setPreferredSize(new Dimension(200, 100));
        
    //layout
    JPanel panel = new JPanel();
    panel.setBackground(Color.darkGray);
    panel.setSize(500,270);
    panel.add(viewswim);
    panel.add(viewgym);
    panel.add(viewbadmin);
    panel.add(viewall);
    
    // add an ActionListener to respond to a button click
    viewswim.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                filtercoaches("swim");  
                frame1.dispose();
            }  
            }); 
    
    viewgym.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filtercoaches("gym");  
            frame1.dispose();
        }  
        }); 

    viewbadmin.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filtercoaches("badminton");  
            frame1.dispose();
        }  
        }); 


    viewall.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filtercoaches("all");  
            frame1.dispose();
        }  
        }); 
                
                

    frame1.add(panel);
    frame1.setLocationRelativeTo(null);
    frame1.setVisible(true);
        
        
        
    }
    
    
 
    
    
    
    
    public void filtercoaches(String type){
        
        
        
        JFrame frame = new JFrame("View Coaches");
        String[] columns = {" ","Coach Id", "Name", "Telephone", "Address",
                            "Swimming", "Gymnestics", "Badminton"};
        Object[][] data = {};
        
 
        DefaultTableModel listTableModel;
        listTableModel = new DefaultTableModel(data, columns);    
        
        Integer tempid;
        String tempname;
        String temptelephone;
        String tempaddress;
        boolean tempteachswimming;
        String tempteachswimmingval;
        boolean tempteachgym;
        String tempteachgymval;
        boolean tempteachbadminton;
        String tempteachbadmintonval;
        
    
        
        for(Map.Entry<Integer, coach> entry:vlcdata.coaches.entrySet()){    
        int key=entry.getKey();  
        coach b=entry.getValue(); 
       
        //=======================================================
                 
        tempid=b.id;
        tempname=b.name;
        temptelephone=b.telephone;
        tempaddress=b.address;
        tempteachswimming=b.teachswimming;
        tempteachgym=b.teachgynnastics;
        tempteachbadminton=b.teachbadminton;        
        
        if (type=="swim"){
                    
                    
                if (tempteachswimming==true){
                    
                    if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                    if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                    if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                    listTableModel.addRow(new Object[]{"View Classs", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                    //JOptionPane.showMessageDialog(null, "Data Saved");
                   
                }

        }
        
        
        if (type=="gym"){

                if (tempteachgym==true){
                    
                    if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                    if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                    if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                    listTableModel.addRow(new Object[]{"View Classs", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                    //JOptionPane.showMessageDialog(null, "Data Saved");
                   
                }
            
            
            
        }
        if (type=="badminton"){


                if (tempteachbadminton==true){
                    
                    if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                    if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                    if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                    listTableModel.addRow(new Object[]{"View Classs", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                    //JOptionPane.showMessageDialog(null, "Data Saved");
                   
                }
                        
            
            
            
        }
        if (type=="all"){

                if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                listTableModel.addRow(new Object[]{"View Classs", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                //JOptionPane.showMessageDialog(null, "Data Saved");
                   
         
        }
                
                
              
                
                
                 
        }
        
        

        //table rendering
        
         
        JTable table;
        table = new JTable(listTableModel);
        table.getColumn(" ").setCellRenderer(new ButtonRenderer());
        
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            
            
            //System.out.println(table.getValueAt(table.getSelectedRow(), 1).toString());
            if (! event.getValueIsAdjusting())
                {
                
                    eclass viewclass = new eclass();
                    viewclass.filterclasses(null,Integer.parseInt(table.getValueAt(table.getSelectedRow(), 1).toString()));
            
                    
                }
           
            
            
        }
        });
        
        
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
 
        JLabel lblHeading = new JLabel("View Coaches");
        lblHeading.setFont(new Font("Arial",Font.TRUETYPE_FONT,24));
 
        frame.getContentPane().setLayout(new BorderLayout());
 
        frame.getContentPane().add(lblHeading,BorderLayout.AFTER_LAST_LINE);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        
        frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
  
         
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
    }
    
 
    
    
    
    
    
    
    public void viewvisitorcoaches(){
        
    JFrame frame1 = new JFrame("View coaches");
    
    frame1.setSize(500,270);
    
    //advanced swimming, gymnastics, badminton
    JButton viewswim = new JButton("Swimming Coaches"); // 
    JButton viewgym = new JButton("Gym Coaches"); // 
    JButton viewbadmin = new JButton("Badminton Coaches"); // 
    JButton viewall = new JButton("View All"); // 
     
    viewswim.setPreferredSize(new Dimension(200, 100));
    viewgym.setPreferredSize(new Dimension(200, 100));
    viewbadmin.setPreferredSize(new Dimension(200, 100));
    viewall.setPreferredSize(new Dimension(200, 100));
        
    //layout
    JPanel panel = new JPanel();
    panel.setBackground(Color.darkGray);
    panel.setSize(500,270);
    panel.add(viewswim);
    panel.add(viewgym);
    panel.add(viewbadmin);
    panel.add(viewall);
    
    // add an ActionListener to respond to a button click
    viewswim.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                filtervisitcoaches("swim");  
                frame1.dispose();
            }  
            }); 
    
    viewgym.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filtervisitcoaches("gym");  
            frame1.dispose();
        }  
        }); 

    viewbadmin.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filtervisitcoaches("badminton");  
            frame1.dispose();
        }  
        }); 


    viewall.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filtervisitcoaches("all");  
            frame1.dispose();
        }  
        }); 
                
                

    frame1.add(panel);
    frame1.setLocationRelativeTo(null);
    frame1.setVisible(true);
        
        
        
    }
    
 
    
    
    
    public void filtervisitcoaches(String type){
        
        
        
        JFrame frame = new JFrame("View Coaches");
        String[] columns = {" ","Coach Id", "Name", "Telephone", "Address",
                            "Swimming", "Gymnestics", "Badminton"};
        Object[][] data = {};
        
 
        DefaultTableModel listTableModel;
        listTableModel = new DefaultTableModel(data, columns);    
        
        Integer tempid;
        String tempname;
        String temptelephone;
        String tempaddress;
        boolean tempteachswimming;
        String tempteachswimmingval;
        boolean tempteachgym;
        String tempteachgymval;
        boolean tempteachbadminton;
        String tempteachbadmintonval;
        
    
        
        for(Map.Entry<Integer, coach> entry:vlcdata.coaches.entrySet()){    
        int key=entry.getKey();  
        coach b=entry.getValue(); 
       
        //=======================================================
                 
        tempid=b.id;
        tempname=b.name;
        temptelephone=b.telephone;
        tempaddress=b.address;
        tempteachswimming=b.teachswimming;
        tempteachgym=b.teachgynnastics;
        tempteachbadminton=b.teachbadminton;        
        
        if (type=="swim"){
                    
                    
                if (tempteachswimming==true){
                    
                    if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                    if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                    if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                    listTableModel.addRow(new Object[]{"Book Visit", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                    //JOptionPane.showMessageDialog(null, "Data Saved");
                   
                }

        }
        
        
        if (type=="gym"){

                if (tempteachgym==true){
                    
                    if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                    if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                    if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                    listTableModel.addRow(new Object[]{"Book Visit", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                    //JOptionPane.showMessageDialog(null, "Data Saved");
                   
                }
            
            
            
        }
        if (type=="badminton"){


                if (tempteachbadminton==true){
                    
                    if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                    if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                    if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                    listTableModel.addRow(new Object[]{"Book Visit", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                    //JOptionPane.showMessageDialog(null, "Data Saved");
                   
                }
                        
            
            
            
        }
        if (type=="all"){

                if(tempteachswimming==true){tempteachswimmingval="Teaching";}else{tempteachswimmingval="-";}
                if(tempteachgym==true){tempteachgymval="Teaching";}else{tempteachgymval="-";}
                if(tempteachbadminton==true){tempteachbadmintonval="Teaching";}else{tempteachbadmintonval="-";}
                    
                listTableModel.addRow(new Object[]{"Book Visit", key, tempname, temptelephone, tempaddress, tempteachswimmingval, tempteachgymval,tempteachbadmintonval});
                //JOptionPane.showMessageDialog(null, "Data Saved");
                   
         
        }
                
                
              
                
                
                 
        }
        
        

        //table rendering
        
         
        JTable table;
        table = new JTable(listTableModel);
        table.getColumn(" ").setCellRenderer(new ButtonRenderer());
        
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            
            
            //System.out.println(table.getValueAt(table.getSelectedRow(), 1).toString());
            if (! event.getValueIsAdjusting())
                {
                
                    visitor viewtime = new visitor();
                    viewtime.viewtimeslots(Integer.parseInt(table.getValueAt(table.getSelectedRow(), 1).toString()));
            
                    frame.dispose();
                }
           
            
            
        }
        });
        
        
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
 
        JLabel lblHeading = new JLabel("View Coaches");
        lblHeading.setFont(new Font("Arial",Font.TRUETYPE_FONT,24));
 
        frame.getContentPane().setLayout(new BorderLayout());
 
        frame.getContentPane().add(lblHeading,BorderLayout.AFTER_LAST_LINE);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        
        frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
  
         
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
    }
}