/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vlc;

import java.awt.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

 
public class  eclass extends user{
public Integer coachid;
public String classroom;
public String classdate;
public String startime;
public String endtime;
public String classtype;
public Integer parseuid=0;

public eclass  (Integer coachid,String name, String classroom,String classdate,String startime,String endtime,String classtype) {
            this.coachid = coachid;
            this.name = name;
            this.classroom = classroom;
            this.classdate = classdate;
            this.startime = startime;
            this.endtime = endtime;
            this.classtype = classtype;
            
    }
    public eclass (){
            this.coachid = null;
            this.name = null;
            this.classroom = null;
            this.classdate = null;
            this.startime = null;
            this.endtime = null;
            this.classtype = null;
    }
    
    public void viewclasses(){
        
    JFrame frame1 = new JFrame("View Classes");
    
    frame1.setSize(500,270);
    
    //advanced swimming, gymnastics, badminton
    JButton viewswim = new JButton("Swimming Classes"); // 
    JButton viewgym = new JButton("Gym Classes"); // 
    JButton viewbadmin = new JButton("Badminton Classes"); // 
    JButton viewall = new JButton("View All"); // 
     
    viewswim.setPreferredSize(new Dimension(200, 100));
    viewgym.setPreferredSize(new Dimension(200, 100));
    viewbadmin.setPreferredSize(new Dimension(200, 100));
    viewall.setPreferredSize(new Dimension(200, 100));
        
    //layout
    JPanel panel = new JPanel();
    panel.setBackground(Color.darkGray);
    panel.setSize(500,270);
    panel.add(viewswim);
    panel.add(viewgym);
    panel.add(viewbadmin);
    panel.add(viewall);
    
    // add an ActionListener to respond to a button click
    viewswim.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                filterclasses("swim",0);  
                frame1.dispose();
            }  
            }); 
    
    viewgym.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filterclasses("gym",0);  
            frame1.dispose();
        }  
        }); 

    viewbadmin.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filterclasses("badminton",0);  
            frame1.dispose();
        }  
        }); 


    viewall.addActionListener(new ActionListener(){  
        public void actionPerformed(ActionEvent e){  
            filterclasses("all",0);  
            frame1.dispose();
        }  
        }); 
                
                

    frame1.add(panel);
    frame1.setLocationRelativeTo(null);
    frame1.setVisible(true);
        
        
        
    }
    
    
    public void filterclasses(String type,Integer cid){
        
        
        
        JFrame frame = new JFrame("View Classes");
        String[] columns = {" ","Class Id", "Coach Name", "Class Name", "Classroom",
                            "Date", "Timings", "Class Type"};
        Object[][] data = {};
        
 
        DefaultTableModel listTableModel;
        listTableModel = new DefaultTableModel(data, columns);    
        
        Integer tempcoachid;
        String tempcoachname="";
        String tempname;
        String tempclassroom;
        String tempclassdate;
        String tempstarttime;
        String tempendtime;
        String tempclasstype;
        
    
        
        for(Map.Entry<Integer, eclass> entry:vlcdata.eclasses.entrySet()){    
        int key=entry.getKey();  
        eclass b=entry.getValue(); 
       
        //=======================================================
                 
        tempcoachid=b.coachid;
        tempname=b.name;
        tempclassroom=b.classroom;
        tempclassdate=b.classdate;
        tempstarttime=b.startime;
        tempendtime=b.endtime;
        tempclasstype=b.classtype;        
        
        if (type=="swim" & cid==0){
                    
                    
                if (tempclasstype=="Swimming"){
                    
                    
                    vlcdata.getcoachname(tempcoachid);
                    tempcoachname=vlcdata.coachdetails.name;
                    
                    
                    listTableModel.addRow(new Object[]{"Register", key,tempcoachname, tempname, tempclassroom, tempclassdate, tempstarttime +" - " +tempendtime ,tempclasstype});
                    //JOptionPane.showMessageDialog(null, "");
                   
                }

        }
        
        
        if (type=="gym" & cid==0){

                if (tempclasstype=="Gymnastics"){
                    
                    
                    vlcdata.getcoachname(tempcoachid);
                    tempcoachname=vlcdata.coachdetails.name;
                    
                    listTableModel.addRow(new Object[]{"Register", key,tempcoachname, tempname, tempclassroom, tempclassdate, tempstarttime +" - " +tempendtime ,tempclasstype});
                    //JOptionPane.showMessageDialog(null, "");
                   
                   
                }
            
            
            
        }
        if (type=="badminton" & cid==0){


                if (tempclasstype=="Badminton"){
                    
                    
                    vlcdata.getcoachname(tempcoachid);
                    tempcoachname=vlcdata.coachdetails.name;
                    
                    listTableModel.addRow(new Object[]{"Register", key,tempcoachname, tempname, tempclassroom, tempclassdate, tempstarttime +" - " +tempendtime ,tempclasstype});
                    
                    
                   
                   
                }
                        
            
            
            
        }
        if (type=="all" & cid==0){

               
                   
                    vlcdata.getcoachname(tempcoachid);
                    tempcoachname=vlcdata.coachdetails.name;
                    
                    listTableModel.addRow(new Object[]{"Register", key,tempcoachname, tempname, tempclassroom, tempclassdate, tempstarttime +" - " +tempendtime ,tempclasstype});
                    //JOptionPane.showMessageDialog(null, "");
                   
                 
         
        }
         
        
        
        

                
                
                
                
        if (type==null & cid>0){

               if (tempcoachid==cid){
                   
                    vlcdata.getcoachname(tempcoachid);
                    tempcoachname=vlcdata.coachdetails.name;
                    
                    listTableModel.addRow(new Object[]{"Register", key,tempcoachname, tempname, tempclassroom, tempclassdate, tempstarttime +" - " +tempendtime ,tempclasstype});
                    //JOptionPane.showMessageDialog(null, "");
                }   
                   
         
        }      
              
                
                
                 
        }
        
        

        //table rendering
        
         
        JTable table;
        table = new JTable(listTableModel);
        table.getColumn(" ").setCellRenderer(new user.ButtonRenderer());
        
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            
             if (! event.getValueIsAdjusting())
                {
                
                    //eclass viewclass = new eclass();
                    //viewclass.filterclasses(null,Integer.parseInt(table.getValueAt(table.getSelectedRow(), 1).toString()));
                    System.out.println(table.getValueAt(table.getSelectedRow(), 1).toString());
                    booking newbooking = new booking();
                    newbooking.newbookings(Integer.parseInt(table.getValueAt(table.getSelectedRow(), 1).toString()),parseuid);
                    frame.dispose();
                }
           
            
            
        }
        });
        
        
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
 
        JLabel lblHeading = new JLabel("View Classes");
        lblHeading.setFont(new Font("Arial",Font.TRUETYPE_FONT,24));
 
        frame.getContentPane().setLayout(new BorderLayout());
 
        frame.getContentPane().add(lblHeading,BorderLayout.AFTER_LAST_LINE);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
        
        frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
  
         
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
    }
    
    
    
    
    
    
    
public void viewreport(){
        
        JFrame framer1 = new JFrame("Class student list");
        
        JTextArea  tdetails = new JTextArea (5,20);
        tdetails.setEditable(false);
        tdetails.setBorder(BorderFactory.createLineBorder(Color.CYAN, 2));
        JScrollPane scrollPane = new JScrollPane(tdetails);
        
        
        
        tdetails.setText("=================================================================\n");
        
        for(Map.Entry<Integer, eclass> entry:vlcdata.eclasses.entrySet()){    
        int key=entry.getKey();  
        eclass b=entry.getValue(); 
       
        //=======================================================
         
        tdetails.setText(tdetails.getText() + "---------------------------------------------------------------------\n");
        
        
                tdetails.setText(tdetails.getText() + "Class name: \t" + b.name +"\n");
                tdetails.setText(tdetails.getText() + "Class room: \t" + b.classroom +"\n");
                tdetails.setText(tdetails.getText() + "Class date: \t" + b.classdate +"\n");
                tdetails.setText(tdetails.getText() + "Class time: \t" + b.startime + " - " + b.endtime +"\n");
                tdetails.setText(tdetails.getText() + "Class type: \t" + b.classtype +"\n");
                
                tdetails.setText(tdetails.getText() + "Class coach: \t" + vlcdata.getcoachnames(b.coachid) +"\n");
        
                
        
                Integer count=0;
                for(Map.Entry<Integer, booking> entry2:vlcdata.bookings.entrySet()){    
                    int key2=entry2.getKey();  
                    booking b2=entry2.getValue(); 
                    if (b2.classid == key){
                        
                       
                        
                        count=count+1;
                   
                   }

                }
                
                tdetails.setText(tdetails.getText() + "Total Students: \t" + count +"\n");
        
               
        tdetails.setText(tdetails.getText() + "\n" + "---------------------------------------------------------------------\n");
        
        
        }

       
        
         
       
        
        
        
        
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framer1.add(scrollPane);
        framer1.setSize(900, 500);
        framer1.setLocationRelativeTo(null);
        framer1.setVisible(true);
        
    
    
    
}  
    
    
    
    







public void viewreport2(){
        
        
     
        JFrame framer1 = new JFrame("Class student list");
        
        JTextArea  tdetails = new JTextArea (5,20);
        tdetails.setEditable(false);
        tdetails.setBorder(BorderFactory.createLineBorder(Color.CYAN, 2));
        JScrollPane scrollPane = new JScrollPane(tdetails);
        
        
        
        tdetails.setText("=================================================================\n");
        
        for(Map.Entry<Integer, eclass> entry:vlcdata.eclasses.entrySet()){    
        int key=entry.getKey();  
        eclass b=entry.getValue(); 
       
        //=======================================================
         
        tdetails.setText(tdetails.getText() + "---------------------------------------------------------------------\n");
        
        
                tdetails.setText(tdetails.getText() + "Class name: \t" + b.name +"\n");
                tdetails.setText(tdetails.getText() + "Class room: \t" + b.classroom +"\n");
                tdetails.setText(tdetails.getText() + "Class date: \t" + b.classdate +"\n");
                tdetails.setText(tdetails.getText() + "Class time: \t" + b.startime + " - " + b.endtime +"\n");
                tdetails.setText(tdetails.getText() + "Class type: \t" + b.classtype +"\n");
                
                tdetails.setText(tdetails.getText() + "Class coach: \t" + vlcdata.getcoachnames(b.coachid) +"\n");
        
                tdetails.setText(tdetails.getText() + "Member List:--"+"\n");
        
        
                Integer count=0;
                for(Map.Entry<Integer, booking> entry2:vlcdata.bookings.entrySet()){    
                    int key2=entry2.getKey();  
                    booking b2=entry2.getValue(); 
                    if (b2.classid == key){
                        
                        vlcdata.getuserdetails(b2.memberid);
                        tdetails.setText(tdetails.getText() + "\t" + vlcdata.userdetails.name  + "\t" + vlcdata.userdetails.telephone + "\t" + vlcdata.userdetails.address +"\n");
        
                  
                        count=count+1;
                   
                   }

                }
                
                tdetails.setText(tdetails.getText() + "Total Students: \t" + count +"\n");
        
               
        tdetails.setText(tdetails.getText() + "\n" + "---------------------------------------------------------------------\n");
        
        
        }

       
        
         
       
        
        
        
        
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framer1.add(scrollPane);
        framer1.setSize(900, 500);
        framer1.setLocationRelativeTo(null);
        framer1.setVisible(true);
        
    
    
    
    
    
    
    
    
    
    
    
    }
    
    
    
    
    
    
    
    
}
