/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vlc;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.*;
import java.util.Map.Entry;
/**
 *
 * @author pc
 */
public class vlcdataNGTest {
    
    public vlcdataNGTest() {
    }

    
    
    
    /**
     * Test of checkmemberclasstime method, of class vlcdata.
     */
    //1,1,"15-05-2019 06:00 PM","notes"
    //1, "Advanced Swimming", "POOL", "15-04-2019","18:00","20:00","Swimming"
    @Test
    public void testCheckmemberclasstime() {
        vlcdata vlcdatatest=new vlcdata();
        System.out.println("checkmemberclasstime");
        
        Integer memberid = 1;
        String tempclassdate = "15-04-2019";
        String tempstarttime = "18:00";
        
        boolean expResult = true;
        boolean result = vlcdatatest.checkmemberclasstime(memberid, tempclassdate, tempstarttime);
        assertEquals(result, expResult);
      }
    
    
    /**
     * Test of checkmembers method, of class vlcdata.
     */
    //members.put (1, new member("Amanda", "12 noway road, hatfield, ha1 2db","078787878"));  
    
    @Test
    public void testCheckmembers() {
        vlcdata vlcdatatest=new vlcdata();
        System.out.println("checkmembers");
        int varuserid = 1;
        boolean expResult = true;
        boolean result = vlcdatatest.checkmembers(varuserid);
        assertEquals(result, expResult);
        
    }

    
    /**
     * Test of addbooking method, of class vlcdata.
     */
    @Test
    public void testAddbooking() {
        vlcdata vlcdatatest=new vlcdata();
        System.out.println("addbooking");
        Integer classid = 122;
        Integer memberid = 122;
        String date = "";
        String notes = "junittest";
        vlcdatatest.addbooking(classid, memberid, date, notes);
        
        Integer counter=0;
        final Set<Entry<Integer, booking>> mapValues = vlcdatatest.bookings.entrySet();
        final int maplength = mapValues.size();
        final Entry<Integer,booking>[] test = new Entry[maplength];
        mapValues.toArray(test);
        counter=test[maplength-1].getKey();
        
        booking testlastentry=vlcdatatest.bookings.get(counter);
        
        String expResult=notes;
        assertEquals(testlastentry.notes, expResult);
     
    }
    
    
    
    
     /**
     * Test of getcoachnames method, of class vlcdata.
     */
    //coaches.put (1, new coach("Peter","089909900","200 way to", true, false, true)); 
   
    @Test
    public void testGetcoachnames() {
        vlcdata vlcdatatest=new vlcdata();
        System.out.println("getcoachnames");
        int varuserid = 1;
        String expResult = "Peter";
        String result = vlcdatatest.getcoachnames(varuserid);
        assertEquals(result, expResult);
        
    }
    
    
     /**
     * Test of getclassstudentlist method, of class vlcdata.
     */
    @Test
    public void testGetclassstudentlist() {
        vlcdata vlcdatatest=new vlcdata();
        System.out.println("getclassstudentlist");
        int classid = 1;
        Integer expResult = 10;
        Integer result = vlcdatatest.getclassstudentlist(classid);
        assertEquals(result, expResult);
       
    }
    
    
    
    
    /**
     * Test of lastmember method, of class vlcdata.
     */
    @Test
    public void testLastmember() {
        vlcdata vlcdatatest=new vlcdata();
        
        System.out.println("lastmember");
        Integer expResult = 15;
        Integer result = vlcdatatest.lastmember();
        assertEquals(result, expResult);
        
        
    }
    
    
    
    
    /**
     * Test of updatemember method, of class vlcdata.
     */
    @Test
    public void testUpdatemember() {
        vlcdata vlcdatatest=new vlcdata();
       
        System.out.println("updatemember");
        Integer user = 1;
        String name = "jtest";
        String address = "jtest";
        String telephone = "jtest";
        boolean expResult = true;
        boolean result = vlcdatatest.updatemember(user, name, address, telephone);
        assertEquals(result, expResult);
        
    }


    
    
    
        /**
     * Test of getclassdetails method, of class vlcdata.
     */
    //eclasses.put (1, new eclass(1, "Advanced Swimming", "POOL", "15-04-2019","18:00","20:00","Swimming"));
    
    @Test
    public void testGetclassdetails() {
        vlcdata vlcdatatest=new vlcdata();
        System.out.println("getclassdetails");
        int varuserid = 1;
        vlcdatatest.getclassdetails(varuserid);
        assertEquals(vlcdatatest.classdetails.name, "Advanced Swimming");
       
        
    }

    /**
     * Test of deletebooking method, of class vlcdata.
     */
    //bookings.put (1, new booking(1,1,"15-05-2019 06:00 PM","notes"));
    
    @Test
    public void testDeletebooking() {
        vlcdata vlcdatatest=new vlcdata();
        
        System.out.println("deletebooking");
        Integer id = 1;
        boolean expResult = true;
        boolean result = vlcdatatest.deletebooking(id);
        assertEquals(result, expResult);
       
    }

    /**
     * Test of gettimeslot method, of class vlcdata.
     */
    //visittimeslots.put (1,"04-05-2019 09:00 AM - 09:20 AM");
   
    @Test
    public void testGettimeslot() {
        vlcdata vlcdatatest=new vlcdata();
        
        System.out.println("gettimeslot");
        int id = 1;
        String expResult = "04-05-2019 09:00 AM - 09:20 AM";
        String result = vlcdatatest.gettimeslot(id);
        assertEquals(result, expResult);
        
        
    }
    

    
}
